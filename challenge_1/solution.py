from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import json
import hashlib

class S(BaseHTTPRequestHandler):
  # Stores messages
  _messages = {}

  def _set_headers(self, code = 200):
    self.send_response(code)
    self.send_header('Content-type', 'application/json')
    self.end_headers()

  def do_GET(self):
      # Parse path. The only valid paths are /messages/<insert message digest here>
      path = self.path.split("/")
      if (not (path[0] == '' and path[1] == "messages")) or len(path) != 3:
        self._set_headers(404)
        self.wfile.write(json.dumps({"err_msg": "Bad URL"})) 
        return
      elif not path[2] in self._messages:
        self._set_headers(404)
        self.wfile.write(json.dumps({"err_msg": "Message not found"})) 
        return
      
      # The message digest was found! Return the original message.
      self._set_headers()
      self.wfile.write(json.dumps(self._messages[path[2]]))

  def do_HEAD(self):
      self._set_headers()
        
  def do_POST(self):
      # Parse path. The only valid path is /messages
      path = self.path.split("/")
      if (not (path[0] == '' and path[1] == "messages")) or len(path) != 2:
        self._set_headers(404)
        self.wfile.write(json.dumps({"err_msg": "Bad URL"})) 
        return

      # Parse request json data. Expected format is {"message": "<insert message here>"}
      request = json.loads(self.rfile.read(int(self.headers.getheader('Content-Length'))))
      if not "message" in request:
        self._set_headers(404)
        self.wfile.write(json.dumps({"err_msg": "Missing 'message' field."})) 
        return
      
      # Generate a digest of the input message. Store it and return the hash.
      response = { "digest": hashlib.sha256(request["message"]).hexdigest() } 
      self._messages[response["digest"]] = request
      self._set_headers()
      self.wfile.write(json.dumps(response))
        
def main(server_class=HTTPServer, handler_class=S, port=8000):
    # Start server
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__": main()
