#!/bin/bash

echo "Starting Server"
python solution.py &>/dev/null &
SERVER_PID=$!
sleep 5 # Busy Wait for server to start. 5s is fine for testing.

echo -e "-- Example One --\n"
( set -x; curl -X POST -H "Content-Type: application/json" 127.0.0.1:8000/messages -d '{"message":"foo"}' )
echo -e "\n"

echo -e "-- Example Two --\n"
( set -x; curl 127.0.0.1:8000/messages/2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae )
echo -e "\n"

echo -e "-- Example Three --\n"
( set -x; curl -i 127.0.0.1:8000/messages/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa )
echo -e "\n"

echo "Killing Server"
kill $SERVER_PID
