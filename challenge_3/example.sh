#!/bin/bash

echo -e "\n-- Example #1 --\n"
(set -x; python solution.py X0)

echo -e "\n-- Example #2 --\n"
(set -x; python solution.py 10X10X0)

echo -e "\n-- Example #3 --\n"
(set -x; python solution.py X)

echo -e "\n-- Example #4 --\n"
(set -x; python solution.py 101010)

echo -e "\n-- Example #5 --\n"
(set -x; python solution.py XX11001100XXX00101XX)
