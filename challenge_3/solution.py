import os
import sys
from Queue import Queue

class Item:
  value = ""
  index = 0

  def __init__(self, value, index):
    self.value = value
    self.index = index

def main():
  '''
  Strategy:

  This is a variation on a breadth-first search.

  1. We create a queue of items, consisting of a string of
     X/1/0 and our current index into that string.
     The queue is initialized with the input string.
    
  2. For each item in the queue, we look for the next X begining
     at <item>.<index>. If no X is found, then we have a valid string
     to output. If there is an X, then we add two new items to the queue.
     One with X replaced by 0 and the other with X replaced by 1.

  3. When the queue is empty, we will have printed all valid strings.

  '''

  # Step 1 - Initialize Queue
  q = Queue()
  q.put(Item(sys.argv[1], 0))
  while not q.empty():
    item = q.get()
    valid_output = True

    # Step 2 - Find valid branches
    for i in range(item.index, len(item.value)):
      if item.value[i] == 'X':
        prefix = item.value[0:i]
        suffix = ""
        if i+1 < len(item.value):
          suffix = item.value[i+1:]
        q.put(Item(prefix + "0" + suffix, i+1))
        q.put(Item(prefix + "1" + suffix, i+1))
        valid_output = False
        break

    # Step 3 - print valid string 
    if valid_output:
      # No X's found
      print item.value

if __name__ == "__main__": main()
