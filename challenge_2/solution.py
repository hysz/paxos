import sys

# Encapsulates an item's name/price
class Item:
  name = ""
  price = 0

  def __init__(self, name, price):
    self.name = name
    self.price = price

def main():
  # State for best match
  best_i1 = 0
  best_i2 = 0
  best_price = 0  
  found_best = False

  # List of all items
  items = []
  
  # Max sum of prices
  capacity = int(sys.argv[2])

  '''
  Strategy:
  1. Read each line of the input file
     Stop reading when price > capacity, as there
     will be no valid match on  the rest of the lines.
  
  2. For each price, calculate the difference between capacity and price.
     Of all the iteams we've read so far, we want to find the one that
     comes closest to this difference. We do this using a binary search.

  3. After the binary search, the value closest to the diff will either
     be the midpoint+1/midpoint/midpoint-1. We search these indices
     for the optimal valid match. If there is no match, we stop
     processing this item but retain it in our list of items.
     
  4. If our pair of items is better than the current best, then we swap.

  5. At the end of this algorithm, we'll either have the best match or
     no match at all.
     
  '''
  with open(sys.argv[1]) as f:   
    for line in f:
      # Step 1 - Parse input line
      line_split = line.split(",") 
      name = line_split[0]
      price = int(line_split[1])
      if price > capacity:
        break

      # Base case. We need at least one other item in the list.
      if len(items) < 1:
        items.append(Item(name, price))
        continue

      # Step 2 - Binary search
      diff = capacity - price
      first = 0
      last = len(items) - 1 
      best_match = items[first]
      found = False
      mid=first
      while first <= last:
        mid=(first + last) // 2  
        if items[mid].price > diff:
          last = mid - 1
        elif items[mid].price < diff:
          first = mid + 1
        else:
          break 

      # Step 3 - Find best index
      best_idx = -1
      i=mid+1
      while i >= mid-1:
        if i >= 0 and i < len(items) and items[i].price + price <= capacity:
          best_idx = i
          break
        i -= 1
      
      # Step 4 - Record if this is best match
      items.append(Item(name, price))
      if best_idx != -1 and (found_best == False or items[best_idx].price + price > best_price):
        best_i1 = best_idx
        best_i2 = len(items) - 1 # this item
        best_price = items[best_idx].price + price
        found_best = True
         
  # Step 5 - Print output
  if not found_best: 
    print "Not possible"
    return
  print items[best_i1].name + " " + str(items[best_i1].price) + ", " + items[best_i2].name + " " + str(items[best_i2].price)

if __name__ == '__main__':
  main()
