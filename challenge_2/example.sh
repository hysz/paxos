#!/bin/bash

(set -x; cat prices.txt)

echo -e "\n-- Example #1 --\n"
(set -x; python solution.py prices.txt 2500)

echo -e "\n-- Example #2 --\n"
(set -x; python solution.py prices.txt 2300)

echo -e "\n-- Example #3 --\n"
(set -x; python solution.py prices.txt 10000)

echo -e "\n-- Example #4 --\n"
(set -x; python solution.py prices.txt 1100)
